package LudumGame;

public class Highscore implements Comparable<Highscore> {
	
	int points = 0;
	String name = null;
	
	@Override	
	public int compareTo(Highscore o) {
		
		if(o.points-points == 0)
			return name.compareTo(o.name);
		else
		return o.points - points;
	}
	
	@Override
	public String toString() {
		
		return name + " " + points;
	}

}
