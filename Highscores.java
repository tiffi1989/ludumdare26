package LudumGame;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class Highscores {

	Set<Highscore> list = new TreeSet<Highscore>();

	public void load() {
		list.clear();

		try {

			String request = "http://patrickschreck.de/highscore.php?action=list";
			URL url = new URL(request);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.connect();

			BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				String[] a = line.split("\\|");
				Highscore h = new Highscore();
				h.name = a[0];
				h.points = Integer.parseInt(a[1]);
				list.add(h);
			}
			rd.close();
			connection.disconnect();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void add(Highscore h) {
		list.add(h);

		try {

			String urlParameters = "name=" + h.name + "&score=" + h.points;
			String request = "http://patrickschreck.de/highscore.php?action=add";
			URL url = new URL(request);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(urlParameters.getBytes().length));
			connection.setUseCaches(false);
			connection.connect();

			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			connection.disconnect();
			System.out.println(connection.getResponseCode() + " " + connection.getResponseMessage());
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
