package LudumGame;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GamePlayState extends BasicGameState{
	
	Image HUD = null;
	Image internetbalken = null;
	Image kaffeebalken = null;
	Image balken = null;
	Image intro = null;
	Image potato = null;
	Image smash = null;
	Image reddit = null;
	Image talk = null;
	Image startend = null;
	Image line = null;
	Image submitscore = null;
	Image submitscore2 = null;
	Image rickroll = null;
	Image kittyviolence = null;
	Image huddark = null;
	Image monitor = null;
	Image ludum = null;
	Image tut1 = null;
	Image tut2 = null;
	Image tut = null;
	Image cuptut1= null;
	
	TextField playername = null;
	
	Highscores highscores = new Highscores();
	
	Animation mum = null;
	Animation quicksmash = null;
	Animation guy = null;
	Animation guyaggro = null;
	Animation cuptut = null;
	
	String keyend = "http://www.youpor...";
	String keyend1 = "http://www.ludumdare.com";
	
	Image[] cup = null;
	String[] talks1 = null;
	String[] talks2 = null;
	ArrayList<String> talks11 = new ArrayList<String>();
	ArrayList<String> talks12 = new ArrayList<String>();

	
	Sound hitinet = null;
	Sound nohitinet = null;
	Sound potatosound = null;
	Sound musicfx = null;
	Sound badkitty = null;
	Sound startscreen = null;
	Sound musicgame = null;
	Sound musicend = null;
	
	float xi = 0;
	float yi = 0;
	float xk = 0;
	float yk = 0;
	
	float scale = 2f;
	
	
	Random random = new Random(System.currentTimeMillis());
	
	
	
	int score = 0;
	int cupint = 0;
	int cupcount =0;
	int quickcount = 0;
	int pauseT = 0;
	int hits = 0;
	int miss = 0;
	int misscache = 0;
	int talkcount = 0;
	int talktime = 0;
	float pauseK = 0;
	int nexttalk = 170;
	int loop = 0;
	int loop1 = 0;
	int loop2 = 0;
	int keyint = 0;
	int keyint1 = keyend.length()*500;
	int keyint2 = 11*500;
	int keyint3 = 0;
	
	
	boolean cat = true;
	boolean keyI = false;
	boolean keyK = false;
	boolean right = true;
	boolean startgamestate = false;
	boolean gameplaystate = false;
	boolean gameoverstate = false;
	boolean pausegamestate = false;
	boolean porngamestate = false;
	boolean quicktimestatepotato = false;
	boolean quicktimestatemum = false;
	boolean quicktimestatewronglink = false;
	boolean ismusicrunning = false;
	boolean gotscore = false;
	boolean talky = false;
	boolean istalking = false;
	boolean submitklicked = false;
	boolean kittyviolencebool = false;

	
	Random rn = new Random();
	int min = 30;
	int max = 160;
	
	float xporn = 0;
	float yporn = 0;
	
	float sporn = 1;
	float alphaporn = 0;
	
	int randomNum = rn.nextInt(max - min +1) + min;
	
	Random rnt = new Random();
	int mint = 0;
	int maxt = 10;
	int rntsave = 0;
	int rntsavecache = 0;
	
	
	
	
	TrueTypeFont trueTypeFont = null;
	TrueTypeFont trueTypeFont1 = null;
	TrueTypeFont trueTypeFont2 = null;
	TrueTypeFont trueTypeFont3 = null;
	TrueTypeFont trueTypeFont4 = null;
	
	int stateID = 0;
	int timer = 180;
	
	
	private enum STATES
	{
		START_GAME_STATE,GAME_PLAY_STATE, GAME_OVER_STATE, QUICKTIME_STATE_POTATO, QUICKTIME_STATE_MUM, QUICKTIME_STATE_WRONGLINK,PORN_GAME_STATE, PAUSE_GAME_STATE;
	}
	
	private STATES currentState = null;
	
	GamePlayState(int stateID)
	{
		this.stateID = stateID;
	}
	
	@Override
	public int getID()
	{
		return stateID;
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame sb) throws SlickException
	{
		super.enter(gc, sb);
		currentState = STATES.START_GAME_STATE;
	}
	
	

	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		
		HUD = new Image("res/HUD.png");
		Image gameplay = new Image("res/gameplayzeugs.png");
		intro = new Image("res/intro.png");
		potato = new Image("res/quicktimepotato.png");
		smash = new Image("res/smash.png");
		mum = new Animation(new Image[]{new Image("res/mum.png"), new Image("res/mum2.png")}, 250);
		quicksmash = new Animation(new Image[]{new Image("res/smash.png"), new Image("res/smash2.png")}, 250);
		guy = new Animation(new Image[]{new Image("res/guy.png"), new Image("res/guy2.png")}, 250);
		guyaggro = new Animation(new Image[]{new Image("res/guyaggro.png"), new Image("res/guyaggro2.png")}, 250);
		cuptut = new Animation(new Image[]{new Image ("res/cup1.png"),new Image("res/cup2.png"),new Image("res/cup3.png"),new Image("res/cup4.png"),new Image("res/cup5.png"),new Image("res/cup6.png"),new Image("res/cup7.png"),new Image("res/cup8.png"),new Image("res/cup9.png"),new Image("res/cup10.png"),new Image("res/cup11.png")}, 200);
		reddit = new Image("res/reddit.png");
		talk = new Image("res/talk.png");
		startend = new Image("res/startend.png");
		line = new Image("res/line.png");
		submitscore = new Image("res/end1.png");
		submitscore2 = new Image("res/end2.png");
		rickroll = new Image("res/rickroll.png");
		kittyviolence = new Image("res/kittyviolence.png");
		huddark = new Image("res/huddark.png");
		monitor = new Image("res/monitor.png");
		ludum = new Image("res/ludum.png");
		tut = new Image("res/gameplayzeugstut.png");
		tut1 = tut.getSubImage(9, 9, 154, 18);
		tut2 = tut.getSubImage(9, 59, 154, 18);
		cuptut1 = new Image("res/cup0_1.png");
		musicgame = new Sound("res/musicgame.ogg");
		musicend = new Sound("res/musicend.ogg");
		
		
		
		
		
		
		
		
	
		
		
		
		
		internetbalken = gameplay.getSubImage(9, 9, 154, 18);
		kaffeebalken = gameplay.getSubImage(9, 59, 154, 18);
		balken = gameplay.getSubImage(12, 79, 3, 16);
		
		Font font = new Font("Comic Sans MS", Font.BOLD, 15);
		Font font1 = new Font("Comic Sans MS", Font.PLAIN, 12);
		Font font2 = new Font("Comic Sans MS", Font.BOLD, 20);
		Font font3 = new Font("Comic Sans MS", Font.BOLD, 30);
		Font font4 = new Font("Comic Sans MS", Font.BOLD, 20);
		
	
		trueTypeFont = new TrueTypeFont(font, true);
		trueTypeFont1 = new TrueTypeFont(font1, true);
		trueTypeFont2 = new TrueTypeFont(font2, true);
		trueTypeFont3 = new TrueTypeFont(font3, true);
		trueTypeFont4 = new TrueTypeFont(font4, true);
		
		hitinet = new Sound("res/hitinet.wav");
		nohitinet = new Sound("res/nohitinet.wav");
		badkitty = new Sound("res/badkitty.ogg");
		potatosound = new Sound("res/potatosound.wav");
		startscreen = new Sound("res/startgame.wav");
		
		playername = new TextField(gc, trueTypeFont3, 250, 130, 300, 50);
		
		cup = new Image[]{
			new Image("res/cup0.png"),
			new Image("res/cup1.png"),
			new Image("res/cup2.png"),
			new Image("res/cup3.png"),
			new Image("res/cup4.png"),
			new Image("res/cup5.png"),
			new Image("res/cup6.png"),
			new Image("res/cup7.png"),
			new Image("res/cup8.png"),
			new Image("res/cup9.png"),
			new Image("res/cup10.png"),
			new Image("res/cup11.png")			
		};
		
		
		
		
		
			
		
		
		
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {		
		
		HUD.draw(0,0);
		trueTypeFont.drawString(150,35, "Internet", Color.black);
		trueTypeFont.drawString(155, 80, "Coffee", Color.black);
		trueTypeFont.drawString(600,40, "Score = "+ score,Color.black);
		trueTypeFont.drawString(600,60, "Hits = "+ hits,Color.black);
		trueTypeFont.drawString(600,80, "Misses = "+ miss,Color.black);
		
		
		internetbalken.draw(233,30, scale);
		kaffeebalken.draw(233, 75, scale);
		
		
		
		
		if(startgamestate)
		{
			if(loop1 == 0)
			{
				musicgame.loop();
				loop1++;
			}
			startend.draw();
		
			trueTypeFont.drawString(260, 195, "<- this is the Internet (yes yes, i know, \"but it is so tiny\"),", Color.white);
			trueTypeFont.drawString(260, 210, "    try to press i when the blue bar is in the green area", Color.white);
			trueTypeFont.drawString(260, 245, "<- this is the coffeemeter, if you press c, the blue bar  ", Color.white);
			trueTypeFont.drawString(260, 260, "    will go to the right, you will get more points on hit,  ", Color.white);
			trueTypeFont.drawString(260, 275, "    but the Internet will move faster!", Color.white);
			trueTypeFont.drawString(260, 350, "<- for every 4 sips of coffee, your trash will get fuller.   ", Color.white);
			trueTypeFont.drawString(260, 380, "    Don't miss on the Internet, it will cost you score  ", Color.white);
			trueTypeFont.drawString(260, 395, "    and you wouldn't mess with the Internet!", Color.white);
			trueTypeFont2.drawString(285, 500, "press Enter to start", Color.white);
			trueTypeFont2.drawString(285, 90, "Please enter your Name", Color.white);
			tut1.draw(100,200);
			tut2.draw(100,250);
			cuptut1.draw(100,0);
			cuptut.draw(100,0);
			
			
			playername.render(gc, g);
			playername.setFocus(true);
		}
		
		if(gameplaystate)
		{
			playername.setFocus(false);
			balken.draw(250 + xi,30 + yi, scale);
			balken.draw(250 + xk,75 + yk, scale);
			trueTypeFont.drawString(250, 5, "Ludum Dare starts in " + (180 + timer/1000) + " seconds", Color.black);
			reddit.draw(0,0);
			trueTypeFont1.drawString(298,195, "http://www.roddit.com ",Color.black);
			trueTypeFont1.drawString(275,230, "Pi = 3,15! ",Color.blue);
			guy.setDuration(0, 450-(int)xk);
			guy.setDuration(1, 450-(int)xk);
			guy.draw(0,0);
			
			if((180 + timer/1000) <= nexttalk)
			{
				talky = true;
				talktime = (180 + timer/1000) - 5;
				nexttalk -= 15;
				istalking = true;
				
			}
			
	
		if(talky)
		{
			talk.draw(0,0);
			trueTypeFont.drawString(630, 220, talks11.get(rntsave), Color.black);
			trueTypeFont.drawString(630, 240, talks12.get(rntsave), Color.black);
			
			
			talkcount ++;
		}
		
		
		if(talktime == (180 + timer/1000))
		{
			talky = false;
			talktime = -1;
			talks11.remove(rntsave);
			talks12.remove(rntsave);
			
			
		}
		
		
		
		
		}
		
		if(quicktimestatepotato)
		{
			reddit.draw(0,0);
			trueTypeFont1.drawString(298,195, "TTTTTTTTTTTTTTTTTTTTTT ",Color.black);
			trueTypeFont1.drawString(275,230, "Pi = 3,15! ",Color.blue);
			potato.draw(0,0);
			trueTypeFont.drawString(250, 5, "Ludum Dare starts in " + (180 + timer/1000) + " seconds", Color.black);
			quicksmash.draw(0, 0);
			if(kittyviolencebool)
			kittyviolence.draw(0,0);
			
			guyaggro.draw(0,0);
			talk.draw(0,0);
			trueTypeFont.drawString(630, 220, "NO POTATO!!", Color.black);
			trueTypeFont.drawString(610, 240, "WHAT ARE YOU DOING!!", Color.black);
			trueTypeFont.drawString(630, 260, "POTATO STAAAHP!!", Color.black);
			
			
		}
		
		if(quicktimestatemum)
		{
			trueTypeFont.drawString(250, 5, "Ludum Dare starts in " + (180 + timer/1000) + " seconds", Color.black);

			reddit.draw(0,0);
			trueTypeFont1.drawString(298,195, "http://www.roddit.com ",Color.black);
			trueTypeFont1.drawString(275,230, "Pi = 3,15! ",Color.blue);
			quicksmash.draw(0, 0);
			mum.draw(0,0);
			trueTypeFont.drawString(40, 180, "GET THE FRIKKING", Color.black);
			trueTypeFont.drawString(40, 210, "TRASH OUTSIDE!!!!", Color.black);
			guyaggro.draw(0,0);
		}
		
		if(quicktimestatewronglink)
		{
			trueTypeFont.drawString(250, 5, "Ludum Dare starts in " + (180 + timer/1000) + " seconds", Color.black);

			quicksmash.draw(0,0);
			rickroll.draw(0,0);
			trueTypeFont1.drawString(298,195, "http://www.yutube.com/r1(kr0ll3d ",Color.black);
			guyaggro.draw(0,0);
			talk.draw(0,0);
			trueTypeFont.drawString(630, 220, "NOOOOOOO!!!!", Color.black);
			trueTypeFont.drawString(610, 240, "WHAT HAVE I DONE!!", Color.black);
		}
		
		cup[cupint].draw(0,0);
		
		if(porngamestate)
		{
			
			
			HUD.draw(xporn, yporn, sporn);
			huddark.draw(xporn, yporn,sporn );
			huddark.setAlpha(alphaporn);
			monitor.draw(xporn, yporn, sporn);
			ludum.draw(xporn,yporn, sporn);
			if(sporn >= 2 && keyint/500 <= keyend.length())
			{
			
			trueTypeFont2.drawString(300,163,keyend.substring(0, keyint/500),Color.black);
			}
			if(keyint1/500 >= 11 && keyint/500 >= keyend.length())
			{
				trueTypeFont2.drawString(300,163,keyend.substring(0, keyint1/500),Color.black);
			}
			if(keyint2/500 <= keyend1.length()&& keyint1/500<=11)
			{
				trueTypeFont2.drawString(300,163,keyend1.substring(0, keyint2/500),Color.black);
			}
			
			
			
			
			
		}
		
		if(gameoverstate)
		{
			HUD.draw(xporn, yporn, sporn);
			huddark.draw(xporn, yporn,sporn );
			huddark.setAlpha(alphaporn);
			monitor.draw(xporn, yporn, sporn);
			ludum.draw(xporn,yporn, sporn);
			trueTypeFont2.drawString(300,163,keyend1,Color.black);
			
			trueTypeFont2.drawString(360, 50, "Game Over", Color.white);
			trueTypeFont2.drawString(200, 223, "Score = " + score , Color.black);
			trueTypeFont2.drawString(200, 278, "Hits = " + hits, Color.black);
			trueTypeFont2.drawString(200, 335, "Misses = " + miss, Color.black);
			trueTypeFont2.drawString(200, 400, "AND DONT PUNCH THE KITTY!!", Color.black);
			trueTypeFont2.drawString(290, 80, "Retry ENTER - Mainmenu ESC", Color.white);
			if(submitklicked)
				submitscore2.draw(xporn, yporn, sporn);
			trueTypeFont2.drawString(538, 240, "Submit", Color.black);
			trueTypeFont2.drawString(543, 270, "Score", Color.black);
			
			

		}
		
		

		
		
		
		
		
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
		
		
		
		
		switch(currentState)
		{
		case START_GAME_STATE:
			submitklicked = false;
			startgamestate = true;
			Input input1 = gc.getInput();
		
			gameoverstate = false;
			
			if(loop == 0)
			{
			talks11.add("Hmm...");
			talks11.add("Know whats funny?");
			talks11.add("What the F*%�)");
			talks11.add("*singing* i wanna be");
			talks11.add("Sleep is for the weak");
			talks11.add("There are only");
			talks11.add("The Potato is a LIE");
			talks11.add("I wonder what this");
			talks11.add("I'm Batman!");
			talks11.add("Sparkling Vampires are");
			talks11.add("Death to Jeoffrey!");
			talks11.add("Forward to the past!");
			talks11.add("Why the heck");
			talks11.add("Are pirates sill");
			talks11.add("Hmm...");
			
			
			talks12.add("I like Toast!");
			talks12.add("--comedies");
			talks12.add("is a Nerdogochi?");
			talks12.add("the very best...");
			talks12.add(" ");
			talks12.add("3 Star Wars movies!");
			talks12.add(" ");
			talks12.add("2 Girls 1 Cup thing is");
			talks12.add("");
			talks12.add("GAY!!!");
			talks12.add("");			
			talks12.add("Where is my towel?");
			talks12.add("does paper beat rock..?");
			talks12.add("in this year?");
			talks12.add("Ludum Dare ftw");
			
			
			loop ++;
			}
			timer = 180;
			score = 0;
			score = 0;
			hits = 0;
			miss = 0;
			misscache = 0;
			xk = 0;
			
			
			
			cupint = 0;
			if(input1.isKeyPressed(Input.KEY_ENTER))
			{
				startscreen.play();
				currentState = STATES.GAME_PLAY_STATE;
			}
			break;
		case GAME_PLAY_STATE:
			Input input = gc.getInput();
			quicktimestatepotato = false;
			quicktimestatemum = false;
			quicktimestatewronglink = false;
			startgamestate = false;
			pausegamestate = false;
			gameplaystate = true;
			kittyviolencebool = false;
			timer -= delta;
			
			
			int randomNumt = rnt.nextInt(talks11.size()-1);
			
			if(istalking)
			{
				
				
				rntsave = randomNumt;
				
				istalking = false;
				
				
			}
			if(xi <= 275 && right)
				xi += 0.2 * delta * 0.02*xk;
			
			if(xi >= 274 && right)
				right = false;			
			
			if(xi>= 0 && right == false)
				xi = Math.max(xi - 0.2f *delta * 0.02f*xk, 0);
			
			if(xi <= 1 && right == false)
				right = true;
			
			if(input.isKeyPressed(Input.KEY_C))
				keyK = true;
				
			
			if(keyK && xk <= 275)
			{
				xk = Math.min(xk + 30, 275 );
				
				if(cupint < 11)
				{
					cupcount += 1;
					if(cupcount == 4)
						{
						cupint += 1;
						cupcount = 0;
						}
				}
					
				keyK = false;
			}
			
			//if(input.isKeyDown(Input.KEY_T))
			//	xi += 0.1 *delta;
			
			if(input.isKeyPressed(Input.KEY_I))
				keyI = true;
				
			
			
			if(keyI && (xi >= 113 && xi <= 154) && gotscore == false)
			{
				score = score + (10 * (1 + (int)xk /10));
				hits += 1;
				hitinet.play();
				keyI = false;
				gotscore = true;
			}
			
			if(xi<113 || xi > 154)
				gotscore = false;
			
			if(keyI && (xi<113 || xi > 154))
			{
				nohitinet.play();
				miss += 1;
				misscache++;
				score = score - (10 * (1+ (int)xk /40));
				keyI = false;
			}
				
			
		
			
			if(xk >= 0)
				xk = Math.max(xk - 0.02f * delta, 0);
			
			
			if((180 + timer/1000) <= 0 && !quicktimestatemum && !quicktimestatepotato && !quicktimestatewronglink)
			{
					musicgame.stop();
					currentState = STATES.PORN_GAME_STATE;
			}
			
			if((180 + timer/1000) <= randomNum && cat && quicktimestatemum==false && quicktimestatewronglink == false && miss % 30 > 1 && cupint <= 10)
			{
				potatosound.loop();
				
				currentState = STATES.QUICKTIME_STATE_POTATO;
				cat = false;
			}
			
			if(cupint == 11)
			{
				potatosound.loop();
				currentState = STATES.QUICKTIME_STATE_MUM;
			}
			
			if(misscache == 30)
			{
				potatosound.loop();
				currentState = STATES.QUICKTIME_STATE_WRONGLINK;
			}
			
		
			
		
			
			
			break;
		
		case QUICKTIME_STATE_POTATO:
			Input input2 = gc.getInput();
			gameplaystate = false;
			quicktimestatepotato = true;
			timer -= delta;
			
			if(input2.isKeyPressed(Input.KEY_SPACE))
			{
				quickcount += 1;
			}
			if(quickcount == 15)
			{
				kittyviolencebool = true;
				badkitty.play(1, 1);
				quickcount += 1;
			}
			if(quickcount == 25)
			{
			
				potatosound.stop();
				quickcount = 0;
				currentState = STATES.GAME_PLAY_STATE;
				
			
			}
			break;
		case QUICKTIME_STATE_MUM:
			Input input3 = gc.getInput();
			gameplaystate = false;
			quicktimestatemum = true;
			timer -= delta;
			if(input3.isKeyPressed(Input.KEY_SPACE))
			{
				quickcount += 1;
			}
			
			if(quickcount == 25)
			{
				cupint = 0;
				potatosound.stop();			
				currentState = STATES.GAME_PLAY_STATE;
				quickcount = 0;
			
			}
			
			break;
		case QUICKTIME_STATE_WRONGLINK:
			Input input4 = gc.getInput();
			timer -= delta;
			gameplaystate= false;
			quicktimestatewronglink = true;
			
			if(input4.isKeyPressed(Input.KEY_SPACE))
			{
				quickcount += 1;
			}
			
			if(quickcount == 25)
			{
				potatosound.stop();			
				currentState = STATES.GAME_PLAY_STATE;
				quickcount = 0;
				misscache = 0;
			
			}
			
			break;
		case PAUSE_GAME_STATE:
			Input input5 = gc.getInput();
			pausegamestate = true;
			
				
			

			if(input5.isKeyPressed(Input.KEY_P))
				currentState = STATES.GAME_PLAY_STATE;
			
			break;
			
		case PORN_GAME_STATE:
			gameplaystate = false;
			porngamestate = true;
			
			if(loop2 == 0)
			{
			loop2++;
			musicend.loop();
			}
			if(sporn >= 2)
				keyint += delta;
			
			if(keyint/500 >= keyend.length())
			{
				keyint1 -=delta;
			}
			if(keyint1/500 <= 11)
			{
				keyint2 += delta;
			}
			if(keyint2/500 >= keyend1.length())
				currentState = STATES.GAME_OVER_STATE;
			
			System.out.println(keyint/500);		
			
			if(sporn<=2)
			{
				xporn = xporn -  (float)0.03 *delta;
				yporn = yporn - (float)0.023 * delta;
				sporn += 0.0001 * delta;
				alphaporn = alphaporn + (float)0.0001 * delta;
	
			}
			
			
				
			break;
			
			
			
		case GAME_OVER_STATE:
			Input input6 = gc.getInput();
			gameplaystate = false;
			porngamestate = false;
			gameoverstate = true;
			talks11.clear();
			talks12.clear();
			boolean insideStartGame = false;
			
			if( ( input6.getMouseX() >= 500 && input6.getMouseX() <= 640) &&
		    		( input6.getMouseY() >= 210 && input6.getMouseY() <= 330) )
		    		insideStartGame = true;
			
			System.out.println(input6.getMouseX() + " " + input6.getMouseY() );
			
			if(insideStartGame && input6.isMousePressed(Input.MOUSE_LEFT_BUTTON))
			{
			if (loop == 1) {
				
				loop = 0;
				submitklicked = true;
				Highscore h = new Highscore();
				h.points = score;
				h.name = playername.getText();
				highscores.add(h);
			}
			}
			if(input6.isKeyPressed(Input.KEY_ENTER))
			{
				currentState = STATES.START_GAME_STATE;
				musicend.stop();
				loop1 = 0;
				loop2 = 0;
			}
			if(input6.isKeyPressed(Input.KEY_ESCAPE))
				sb.enterState(Nerdogochi.MAINMENUSTATE);
			
			break;
			
		}
		
		
		
		
		
		
		
		
			
		
		
		
		
		
		
	}
	
	public int getRandomNumber(int i){
		return random.nextInt(i);
	}
	


	
	

}
