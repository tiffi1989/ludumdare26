package LudumGame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Nerdogochi  extends StateBasedGame{
	
	public static final int MAINMENUSTATE = 0;
	public static final int GAMEPLAYSTATE = 1;
	
	public Nerdogochi()
	{
		super("The minimalistic life");
		
	}
	
	
	public static void main(String[] args) throws SlickException
	{
		AppGameContainer app = new AppGameContainer(new Nerdogochi());
		
		app.setDisplayMode(800, 600, false);
		app.setShowFPS(false);
		app.start();
	}

	@Override
	public void initStatesList(GameContainer gameContainer) throws SlickException {
		this.addState(new MainMenuState(MAINMENUSTATE));
        this.addState(new GamePlayState(GAMEPLAYSTATE));		
	}

}
