package LudumGame;


import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class MainMenuState extends BasicGameState{
	
	Image background = null;
	Image startGameOption = null;
	Image exitGameOption = null;
	Image highscores = null;
	
	TrueTypeFont trueTypeFont = null;
	TrueTypeFont trueTypeFont1 = null;
	
	private static int menuX = 20;
	private static int menuY = 550;
	
	boolean isMusicRunning = false;
	
	
	Sound musicfx = null;
	Sound klickfx = null;
	
	int stateID = -1;
	
	MainMenuState(int stateID)
	{
		this.stateID = stateID;
	}
	

	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		background = new Image("res/hud.png");
		Image menuOptions = new Image("res/menuoptions.png");
		klickfx = new Sound("res/klickfx.wav");
		musicfx = new Sound("res/musicfx.ogg");
		Font font = new Font("Comic Sans MS", Font.BOLD, 15);
		Font font1 = new Font("Comic Sans MS", Font.BOLD, 40);
		GamePlayState gameState = (GamePlayState)sb.getState(Nerdogochi.GAMEPLAYSTATE);
		gameState.highscores.load();
		
		trueTypeFont = new TrueTypeFont(font, true);
		trueTypeFont1 = new TrueTypeFont(font1, true);
		
		
		startGameOption = menuOptions.getSubImage(0, 0, 280, 50);
		exitGameOption = menuOptions.getSubImage(0, 50, 280, 50);
		highscores = menuOptions.getSubImage(0, 100, 300, 50);
		
		
		
		
		
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		background.draw(-300, -260, (float)2);
		startGameOption.draw(menuX, menuY);
		exitGameOption.draw(menuX + 630, menuY);
		trueTypeFont.drawString(275, 150 ,"Highscores:", Color.black);
		trueTypeFont1.drawString(50, 20 ,"Nerdogochi", Color.black);
		trueTypeFont.drawString(50, 80 ,"The minimalistic life", Color.black);
		
		
		GamePlayState gameState = (GamePlayState)sb.getState(Nerdogochi.GAMEPLAYSTATE);
		
		int i = 0;
		for (Highscore h : gameState.highscores.list) {
			trueTypeFont.drawString(300, 175 + 25*i, h.toString(), Color.black);
			
			trueTypeFont.drawString(275, 175 + 25*i,Integer.toString(i+1), Color.black);
			i++;
			if (i == 10) {
				break;
			}
		}
		
		
		
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
		Input input =gc.getInput();
		
		if(isMusicRunning == false)
		{
			musicfx.loop();
			isMusicRunning = true;
		}
		
		
		
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();
		
		boolean insideStartGame = false;
		boolean insideExitGame = false;
		
		if( ( mouseX >= menuX && mouseX <= menuX + startGameOption.getWidth()) &&
	    		( mouseY >= menuY && mouseY <= menuY + startGameOption.getHeight()) ){
	    		insideStartGame = true;    		 		
	    	}else if( ( mouseX >= menuX + 630 && mouseX <= menuX + 630 + exitGameOption.getWidth()) &&
	    			  ( mouseY >= menuY && mouseY<= menuY + exitGameOption.getHeight())){
	    		insideExitGame = true;
	    	}
	    	
		if(insideStartGame){
    		
    		if ( input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
    			musicfx.stop();
    			sb.enterState(Nerdogochi.GAMEPLAYSTATE);
    			klickfx.play();
    		}
    		
    		}
		if(input.isKeyPressed(Input.KEY_ENTER))
		{
			musicfx.stop();
			sb.enterState(Nerdogochi.GAMEPLAYSTATE);
		}
		if(insideExitGame)
    	{    		
    		if ( input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON) )
    			gc.exit();
    	}
		
		if(input.isKeyPressed(Input.KEY_ESCAPE))
			gc.exit();
		
		
		
	}

	@Override
	public int getID() {
		
		return stateID;
	}
	

}
